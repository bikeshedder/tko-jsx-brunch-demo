// See http://brunch.io for documentation.
exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'}
};

exports.plugins = {
  babel: {
    "presets": [["env", {
      "targets": {"browsers": "chrome 52"}
    }]],
    "plugins": [
      "babel-plugin-transform-jsx"
    ]
  }
}
