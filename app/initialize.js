import tko from '@tko/build.reference/dist/build.reference.es6'

import './demo-component.jsx'

document.addEventListener('DOMContentLoaded', function() {
    tko.applyBindings()
})
